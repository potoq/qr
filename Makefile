start:
	@docker run --rm -it \
	-v $(CURDIR):/local/project \
	-w /local/project \
	-p 3333:80 \
	node:10-alpine \
	npm run start