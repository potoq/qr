'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const qr = require('qr-image');

const app = express();
app.use(bodyParser.json());
app.get('/qr', (req, res) => {
  const text = req.query.text || 'sample';
  const img = qr.image(text);
  res.setHeader('Content-Type', 'image/png');
  img.pipe(res);
});
app.post('/', (req, res) => {
  const img = qr.image(req.body.text);
  res.setHeader('Content-Type', 'image/png');
  img.pipe(res);
});

app.listen(80);
